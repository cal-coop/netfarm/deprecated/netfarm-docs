# Codecs

Codecs are transformations applied to an atom to create a new datatype from
the string-based representation of Netfarm objects, or to overcome limitations
in the wire format Netfarm uses. 

There are a few codecs already defined:

- `base64`: encode a string using base64
- `base64-data`: encode a byte array using base64
- `integer`: encode a (decimal) integer
- `ref`: encode a reference to another object by hash

Codecs are encoded left to right, and decoded right to left.
