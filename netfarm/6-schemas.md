# Schemas

Schemas provide coherency and meaning to objects in Netfarm, and are
similar to classes in object-oriented programming. Schemas contain slot
descriptions and trait descriptions.

Slots are created using the `slots` slot, and traits are created using the
`traits` slot. Both use the `lists` reader type, which is useful for creating
domain specific formats for programs.

## Slot descriptions

## Trait descriptions
