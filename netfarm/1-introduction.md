<!--
---
title: 'Netfarm: an extensible decentralised protocol'
author: "Dale O'Really (theemacsshibe)"
---
-->

# Netfarm: an extensible decentralised protocol (version -1)

Welcome to the Netfarm documentation. Netfarm adds very useful components that
are not included in the minimal core of cl-decentralise. The system provides:

- storage formats
  - binary-aware formats for storing plain text data
  - zlib compression and base64
  - key-value and triple-store relationships
- a meta-object protocol
  - schemas (similar to classes) for parsing some formats and verification,
    which are also netfarm objects.
  - schemas are converted to CLOS classes and blocks are converted to instances
    of those classes.
- a key store
  - ECDSA verification and ECDH shared key derivation
- verification
  - key lookup and signature checks on objects' representation
  - verifying objects can be parsed correctly

These haven't been implemented yet (see `README.md`'s design choices for why).
Thus, I name this version of the documentation version negative one (-1), since
the earliest release in code should be close to zero and positive.
