# meta object protocol

Netfarm has a meta-object protocol, or MOP. "Meta-object protocol" is a mouthful, 
but it is the most important component of a dynamic system such as Netfarm.
A MOP allows the user to define classes (or in our case, schemas) which define
other objects which are objects themselves.

Simply put, a schema is an object, and an object is defined by a schema.
However, a schema is also defined by a schema so the schema-defining schema
will have issues being defined if an environment for defining schemas isn't
prepared yet. There are two solutions I have for this:

- the schema-defining schema uses a "bootstrap" hard-coded schema, or
- the schema-defining schema is hard-coded.

The bootstrap would allow the SDS to create different schemas, which may or may
not be useful. I'll assume the SDS is the one we'll provide in that case -- you
should be able to extrapolate what to do if you are writing a new SDS.
