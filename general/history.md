# Why was Netfarm made?

Netfarm is a distributed -- not federated, no, really distributed -- protocol
for storing and accessing objects. Netfarm is probably more than the sum of
its parts, and also has some sendimental baggage attached to it too.

# History

Please note, this section of the documentation is written fairly informally and
contains several obscenities. It may be considered as a directed rant more than
a technical description or biographical piece.

# nettle: a distributed data store

Netfarm's first incarnation was in a Python hack written in September of 2017.
The original name for it was "Nettle", but we found out that had a funny name
clash with [a GNU cryptographic library](https://www.lysator.liu.se/~nisse/nettle/).
The name still stuck as it'd be very hard to confuse the two projects.

The small hack did grow into a half-working program, which had several useful
features going for it: signing using ECDSA, a basic object representation
(if a hashtable counts), and the basic text format still mostly used in Netfarm.

Nettle grew out of a website called [Raddle](https://raddle.me). It was made
after the owner got pissed after a subreddit called LeftWithSharpEdge was
banned from Reddit. LWSE was pretty cool, had a lot of tendencies and
diversity in thought and backgrounds and was overall a fun site to talk crap on.

Raddle, after a short explosion in growth after it was founded, became very
dull and homogenous. The most homogenity you can get is if every member is
the same member, which held true that time since the admin had many, many
sockpuppet accounts they were using to create flame wars. During that time, the
Nettle group amassed and we tried to figure out what was happening.

Around a month after, around June of 2018, the administrator had to own up
since the programmer was furious about what they were doing. As is typical
style for that kind of bullshittery, it mysteriously disappeared into the bit
bucket but [archive.is](https://archive.fo/MwqCL) has a copy if you'd like to
read what happened.

A very productive period followed, as a fella named *leftous* replaced *ziq*
as (lead? top? chief? leading...) administrator. This was very nice. No one was
being attacked, people were open to new developments and strategies for making
sure power trips didn't happen again and other nice things. Nettle got a Common
Lisp port after I decided I was good enough at it.

If this period persisted, Nettle would have probably survived, but it did not
persist. Another issue could have killed Nettle too: it could not synchronise
with other nodes which was certainly not useful for a program designed to be
distributed over many nodes. Nettle died after its Lisp port reached version
0.8.1, when some of its nicest features, like the abstract object renderer,
were created.

# cl-decentralise/netfarm/???: a modular distributed object store

After a break to get her mental health back in shape, I started listening
around for ideas for a redo of Nettle. 

One of the ideas was to split up the protocol, so that components could be created 
individially, which could attract other developers, and would speed up development 
as they could be tested alone. This led to the creation of cl-decentralise, the
backbone for the new system which allowed for "blocks" of text and metadata to
be bounced through nodes, eventually sending everyone a copy of every object.

While lurking on #lisp for a while, I met a professor named Robert Strandh, who
was working on the CLOS implementation for [SICL](https://github.com/robert-strandh/SICL),
which is a set of modules for creating a Common Lisp system, including an
implementation of the Lisp reader, a framework for creating a compiler (Cleavir)
among implementation of most of the rest of the standard and other 
taken-for-granted extensions like the Meta Object Protocol. I hadn't really done
much object-oriented programming, since in Python it was all boilerplate code
and C++ and Java looked too complex; but he convinced me that CLOS wasn't all
that bad, and indeed it's quite fantastic. My goal would then be to get as much
of CLOS in a distributed system.

Of course, I didn't know what to do after Netfarm and cl-decentralise were
finished. What would you do with a distributed hash-table and object store?
The original plan was to recreate a forum, but in the final days of Nettle I
realised I had a much more powerful tool for creating multiple flexible programs
on one network.
