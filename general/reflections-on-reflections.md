# Reflections on "Reflections: the ecosystem is moving"

[Signal's blog has to justify their weirdness somehow.](https://signal.org/blog/the-ecosystem-is-moving/)
Apparently any decentralised (even federated) service is doomed to old age,
which is a very damning statement. That doesn't seem right at all, but Moxie
does make some points which are scary at first. I don't think he's right at all,
and the post is a load of FUD.

## Dead standards versus dead communities

Dead standards and dead communities are not the same issue, as a "dead" standard
could be behind a thriving community which has extended past the standard.
Despite hardly moving, many slow-moving protocols like IRC and XMPP still
survive after growing cobwebs, corporate butchering and other atrocities against
the commons happen around it.

### People actually want standardisation

The benefit Moxie claims to be exploiting from centralised systems is that
you can push out updates and people will actually use them. That may be true,
but users are at a loss if they cannot agree on enough standards to communicate
or otherwise share information. For example, 
[Closer to MOP](https://github.com/pcostanza/closer-mop) is supported by plenty
of CL implementations with often very little manipulation required to
comply with the standard set by the
[infamous *The Art of the Metaobject Protocol* book](https://en.wikipedia.org/wiki/The_Art_of_the_Metaobject_Protocol).
Other standards like *Bordeaux threads* and *Gray streams* have seen widespread
implementation, despite not being in the ANSI Common Lisp standard or other
authoritative documentation. De-facto standards such as these are very popular
simply because everyone can use them, creating a kind of
[network effect](https://en.wikipedia.org/wiki/Network_effect) in a userbase.
This effect pushes people into standards, despite there not always being formal 
specifications or a group designated to set these standards.

## Promoting hacking and extendability

Unfortunately, you do have to promote the development of extensions to your
protocol. Moxie points out "XMPP [has] ... limited support for rich media",
but I've happily sent pictures and videos around to people on it, despite those
being rather bland data types. (I'll get to why Moxie might have lost later.)
XMPP does have promotion of standards through XEPs (XMPP Extension Proposals)
which is a very good move for the developers; any project leaders which wish to
survive should consider some platform or method for supporting extensions and
advancements for their project.

He attributes his issues to "XEPs that aren't consistently applied anywhere", 
but if someone wants to run a server or entry point into a federated network 
they should be up-to-date on new extensions as not doing so repels users.
This issue is equivalent to a scenario such as "some Melbournians don't
shower and other Melbournians avoid them cause they stink, and now they
don't know if the people they meet up with have showered"; the client expects
and will more than often receive some level of decency and care from the
operators. Thanks to the network effect previously mentioned, users of inferior
servers will move to better ones, probably the ones that their connections use.

## Timely and timeless systems

Systems may or may not be timely. Something like B or even C may be timely,
and really only applied to one use-case or scenario ("BCPL's syntax is too
wordy and won't fit in main memory" or "B doesn't let us use the smaller
types our new machine has"). A lot of federated and non-federated systems are
timely, and I'd argue these are the most successful as they can be marketed
very easily and directly to consumers. Moxie makes a good point in that
"cannibalizing a federated application-layer protocol into a centralized service
is almost a sure recipe for a successful consumer product today", iff you
consider the success as how many people use the platform, how many venture
capitalists you impress or how many dollars you can reel in from selling user
data. (Did I mention WhatsApp was bought out two years before the Reflection
post was made? Moxie's just jealous.)

Other systems are "timeless", and probably don't go out of date quickly. Dare
the smug Lisp weenie in me come out (hey, this is a Common Lisp project, what
did you expect?), Lisp is pretty timeless. If I show you 
[some Interlisp](https://github.com/PDP-10/its/blob/master/src/lspsrc/funcel.3#l54), 
you can probably read it. With some squinting, you could probably read some Lisp from
[jmc's original Lisp paper](http://www-formal.stanford.edu/jmc/recursive/node3.html).
Anything sufficiently detached from current contexts and focuses on allowing
itself to be modified for future uses is good material for timelessness; no
tears would be shed when threads get ditched in favour of naturally parallel
processes or the next tool for parallelism from timeless designers.

## Backwards and forwards compatibility

Moxie took another one from WhatsApp, well actually more Facebook, but I think
that belief is nasty: that it's fine to be rude about forcing updates on users.
Without thought for backwards compatibility, odds are you're giving the user 
Hobson's choice as they must use the changes to continue using your product; 
there is no other choice should they wish to use your program. However, you
often can't help them as a critical bug may have had to be avoided by changing
protocol, so this may be a moot point. I still should advocate for the
importance of at least issuing partial support for old clients nonetheless.

Similarly, ignoring the possibility for forwards compatibility means the user
gets cut off more from new features. One idea in the old Nettle project was for
HTTP/HTML-emitting renderers to apply a similar transformation to XSLT, or
DSSSL if we look closer to the s-expression based format Nettle could store.
(I cannot find information on DSSSL as link rot has taken out all the
resources on it, so I can't verify that it truly is relevant.) With a layer of
indirection, you can just push out "code" (well, it can be Turing complete if
you want) to add new features and types to a program. The "interpreter pattern"
would be relevant here; but a smart programmer may even choose to put in some
level of native code compilation or bytecode if they are concerned about
transaction speed.

## Conclusion

This is quite a harmful way of looking at development in decentralised systems. 
I think I've debunked all the "benefits" Moxie presents in this writeup. Maybe
he should try out for Microsoft's marketing department, I hear they're looking
for bullshitters.
