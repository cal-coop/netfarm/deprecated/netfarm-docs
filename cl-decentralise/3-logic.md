# node software: decentralise.asd

Loading this system gives you the `decentralise` package. You should make a 
`system` subclass with the following methods:

- get-item (name) -> block, version, channels: a function which takes a name and 
returns the block and metadata associated with the name. 
- list-generator (system) -> closure -> name, version: a function which returns 
a function which returns each name-version pair stored, and NIL when this list 
is exhausted.
- put-item (name data version channels) -> nothing: a function which sets the
version of `name` to `version`, the channels of `name` to `channels` and the data
of `name` to `data`. If `name` does not exist, it should be created. If the object
cannot be put, a condition should be raised and the condition report will be used
as the error message.

These optional values may be set too through accessors with the same names:

- port: the port which the daemon runs on. Defaults to 1892.
- sync-limit: the maximum connections that node discovery will contribute to.
  Node discovery will stop at the limit, but `connect-to` and the server don't
  care. Defaults to 70.
