<!--
---
title: 'CL-Decentralise: a decentralisation layer for your hacks'
author: "Hayley Patton (theemacsshibe)"
---
-->

# CL-Decentralise: a decentralisation layer for your hacks

## Deprecation notice

cl-decentralise is deprecated in favour of cl-decentralise2 for a few reasons:

- cl-decentralise2 can handle multiple wire formats with one system
- cl-decentralise2 has better abstractions, splitting system protocols and 
  connection protocols apart
- cl-decentralise2 will likely be more efficient with many connections as it
  uses one "thinking" thread as opposed to cl-decentralise's thinking thread per
  connection
- cl-decentralise2 has better defined synchronising semantics

## Introduction

cl-decentralise is a simple library for synchronising data between machines.
The purpose of cl-decentralise is to provide a simple, scaleable abstraction
for decentralised programs that doesn't require recreating synchronisation and
node discovery techniques. The rest of your program is a layer above
cl-decentralised which provides storage and verification facilities,
which saves you, the developer, time and allows to develop and experiment with
whatever protocols and concepts you are creating.

cl-decentralise is designed to fit many use cases, but we are developing it to
be the backbone of our Netfarm system, which will provide a meta-object
protocol, cryptographic operations and some data formats for securely sending
around information. The protocol is straightforward and doesn't use any form
of binary transportation, so everything is debugabble and testable by a human
or a program written in any language.

The sections are ordered roughly in order of complexity and layers: higher
level descriptions and concepts are presented in earlier sections and low level
specifics are in later sections. Happy hacking!
