<!--
---
title: 'Netfarm Documentation Build Intructions'
author: 'Ian Mitchell (Comrade Pingu)'
---
-->

# Netfarm Documentation

Welcome to the Netfarm docs. Here, you can find virtually anything you want about NetFarm software. The goal here is to make a [readthedocs.io](readthedocs.io)-like center for all of the documentation. Documentation for specific programs will be put into directories, and will follow a folder structure like this:

```
-doc+
    |
    |
    +-program+
             |
             +: 1-file.md
             +: 2-file.md
             +: n-file.md
```

Then, when it comes time to compile, mdtools can be used to merge all of the markdown files into one single file. The reason for this is that there can be separate pages for separate programs, but then can come together as needed. However, this is not guaranteed to keep out clutter. So, try to follow a common naming scheme that follows the section number and title. If the sections hit higher than the tens place, instead of writing `10-file.md`, write `91-file.md`. This will make sure that the sections follow in a sequential order. The more places, the more 9s to add (i.e. `991-file.md`, `9991-file.md`, `9999-file.md`).

The docs here are compilable with pandoc---html and pdf suggested. 

# Dependencies

## Pandoc Compiling

* [Pandoc](https://github.com/jgm/pandoc)
* [Mdtools](https://github.com/JeNeSuisPasDave/MarkdownTools) (`pip install mdtools`)

## API Usage

* [Gitlab API](https://docs.gitlab.com/ee/api/)
* [Python-Gitlab](https://github.com/python-gitlab/python-gitlab)

# Compiling

## Metadata

When writing a markdown file, it should come as no surprise that you should put in the YAML-Metadata. All you need to include is the title of the document. However, putting your name in along with other contributor names (if they did not put them in) is also suggested. The date is unnecessary, but may be useful for version control.

Metadata should look something like this:

```md

---
title: <YOUR TITLE HERE>
name: <YOUR NAME>
date: <WHO CARES?>
---

```

**NOTE:** *Your metadata **must** come before the start of the document! It also must be between two lines that contain '`---`'. Think of it like a header in LaTeX.*

## Styles

In case if you can not see it, there is a directory called `.styles`. It contains all of the styles used for compiling documentation. 

## Single document files

- **PDF:** 
	- `pandoc <filename>.md --number-sections --toc --listings -o <filename>.pdf`

- **HTML:** 
	- `pandoc -t html5 --self-contained --mathml --css .styles/html.css <filename>.md -o <filename>.html`

You can use webtex or katex. Webtex is much better for drafting, and is *much* quicker to compile; but it only compiles as pictures. Katex compiles as copyable text, which makes it better for a finished product.

### Compiling to PDF

## Metadata

When writing a markdown file, it should come as no surprise that you should put in the YAML-Metadata. All you need to include is the title of the document. However, putting your name in along with other contributor names (if they did not put them in) is also suggested. The date is unnecessary, but may be useful for version control.

Metadata should look something like this:

```md

---
title: <YOUR TITLE HERE>
name: <YOUR NAME>
date: <WHO CARES?>
---

```

**NOTE:** *Your metadata **must** come before the start of the document! It also must be between two lines that contain '`---`'. Think of it like a header in LaTeX.*

## Styles

In case if you can not see it, there is a directory called `.styles`. It contains all of the styles used for compiling documentation. 

## Single document files

- **PDF:** 
	- `pandoc <filename>.md --number-sections --toc --listings -o <filename>.pdf`

- **HTML:** 
	- `pandoc -t html5 --self-contained --mathml --css .styles/html.css <filename>.md -o <filename>.html`

You can use webtex or katex. Webtex is much better for drafting, and is *much* quicker to compile; but it only compiles as pictures. Katex compiles as copyable text, which makes it better for a finished product.

### Compiling to PDF

It is suggested that when you compile to pdf that you use the [eisvogel template](https://github.com/Wandmalfarbe/pandoc-latex-template) for pandoc. Custom metadata for this document includes: `titlepage: [true/false]`, `toc-ownpage: [true/false]`.
