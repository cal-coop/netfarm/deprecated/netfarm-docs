<!--
---
title: 'Netfarm Documentation'
author: 'Netfarm devs'
---
-->

# Why Netfarm?

I'll drill you through the main differences between some popular decentralised
projects and Netfarm/cl-decentralise:

| Name     | License | Primary use                   | Distribution model                    | Data usage         |
|----------|---------|-------------------------------|---------------------------------------|--------------------|
| Netfarm  | CSL     | Forums and chatting I suppose | Fully distributed on demand           | As much as needed  |
| Matrix   | Apache  | Chatting                      | Stuck to homeserver which "federates" | Your messages only |
| Ethereum | GPLv3   | Cryptocurrency                | Proof-of-fossil-burn with contracts   | Entire blockchain  |
| Tox      | GPLv3   | Chatting                      | Fully distributed with routing model  | Others' messages   |
| IPFS     | MIT     | A very scaleable file system  | Peer-to-peer file transfer            | "Seeding" files    |

This is not a very good summary of what Netfarm does, and is probably subject
to my very dry sense of humour and wit. You should probably read on.

# Design Philosophy

> *HAHAHAHAHA HOW IS CENTRALIZATION REAL just decentralize it like lmao*
>
> *– Ian Mitchell*

If compared to the archetypes in [Worse is Better](https://www.jwz.org/doc/worse-is-better.html),
we'd like to think Netfarm follows the MIT/"right thing" design. Client programs
shouldn't have to dive into string parsing and handling themselves, and instead
we should provide proper interfaces (for CL, CLOS classes and objects) and tools
to eliminate patterns in code that hinder development (again CL-specific, the
event-loop macro). Development will naturally be slow, as time will be taken to
decide on *the Right Thing* after a lot of thinking is done.

Possibly attempting to attract venture capitalists and/or lusers, many new
projects seem to implement the eye-catching stuff first and then try to
"port" it into a decentralised system. That's a
[terrible strategy](https://www.youtube.com/watch?v=z3RBOTY771M), as not
everything is trivially replicated for use in a decentralised network. Fancy
design sugar can come after a solid, stable core is created which is able to
host the useful features. I was going to make a bad ripoff of the Alan Perlis
quote on syntactic sugar but I couldn't think of anything. :c

Federation is also a big no-no, as that merely splits load over a set of
servers. Most federated projects add insult to injury and require you to be
stuck to one server or provider, which [isn't very nice either](https://matrix.org).

A bag of bits isn't a useful model. Data actually has value as well as just
characters and numbers. When someone gets an object, they expect more than just
text. A book isn't a vector of words, it has two (or more) covers, a title and
so on. Tangible models like these can map to distributed systems, and facilitate
better verification and type-checking of data. More types can be created as well,
including references and triple-stores; garbage collection would even be possible
to prune useless data for users who only need some parts of the network.

Data isn't a scarce resource either. Proof of work implies that there is only 
so much data to go around. Storage is really cheap nowadays [^1], so storage
isn't a problem. Throughput isn't an issue, either. With a good Internet 
connection, Ethereum can achieve 2,500 transactions per second on paper with 
a specially set up network. That's decent, but we can do better, even on 
worse network conditions.

# License Summary

All Netfarm projects are licensed under the [Cooperative Software License](https://lynnesbian.space/csl/).
While this is a totally free license modeled after the
[*Telekommunist Manifesto*](http://media.telekommunisten.net/manifesto.pdf),
it is important to note that *this license is not
[GNU GPL](https://www.gnu.org/licenses/gpl-3.0.en.html) compatible.*
You are *not* allowed to sell any Netfarm software under *any* circumstances.
However, worker-owned cooperatives are permitted to use the software for
monetary gain as long as money and benefits are distributed to the workers
of the cooperative (the people that do the most work with the software should
get the most from it). 

Much like in the [GNU AGPL](https://www.gnu.org/licenses/agpl.html), you are
required to have a distribution of the software's source code if it is being
run on a server. If that's an issue, you're free to clean-room implement the
Netfarm code yourself. [Here's a good starting point.](https://portacle.github.io/)
Have fun.

## Why that license? (Political background)

As of now (September 30, 2018), all developers of Netfarm are communists of some
form. (There are two Marxists, an ancom and an egoist for anyone counting at home.)
We all believe that most GPL and GPL compatible licenses do not provide enough
protection to workers. As such, we will not use said license(s) for our software.
This is not to say that those licenses are horrible—far from that. We just
see that an alternative license works with our political views better.

As communists, we think that the GPL does not only provide little protection to workers,
but also in fact open up the door to venture capital. Take for example [Tesla](https://github.com/teslamotors),
Elon Musk's little project. Despite being a free software project, running under mainstay
free software licenses like the MIT license, the Apache 2.0 license, and more, it still is
used as Musk's money making project. (**NOTE:** Notice the fedora on one of the Tesla devs 🤔)

By allowing capital, the GPL inhenrently opens the door to the use of proprietary software. 
Programmers, hackers, computer scientists, software engineers, etc., have no natural state. But,
if there *was* such a thing, it would have been something like the MIT AI Lab all those years ago
when RMS ran free through it, probably saying some long-winded paragraphs about why Lisp is good
for the soul. (Granted, most of the free-software community is somewhat like that nowadays.) 
Proprietary software was created when the well meaning, talented programmers from the MIT AI Lab
were infused with *capital*—as in Marx's *Das Kapital*. The goal of the cooperative license is to
prevent this—keep programmers as *laborers*, not *workers*. 