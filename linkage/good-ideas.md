# Some ideas that (c|sh)ould be implemented:

- Fancier value lookup: instead of `(GETHASH "segment" OBJECT)`, make a
  SLOT-VALUE clone. It might take two "slot" arguments, one for segment name
  and another for optional value in the table.
  - Better yet, somehow make SLOT-VALUE operate normally-ish on Netfarm objects.
    SLOT-VALUE isn't a generic function so we can't write a handler for those,
    but there are two options for overriding it:
    - Use SLOT-VALUE-USING-CLASS and bring in AMOP. I don't see why not, it's a
      very nice tool and only very old, mostly pre-AMOP Lisp implementations
      lose.
    - Hack over SLOT-MISSING, which is a generic function we can write new
      methods on. Why was that included in the ANSI CL spec and not the rest of 
      AMOP? Who knows.
- Lazy parsing: somehow only parse segments when they're looked up. Requires a
  proper Netfarm object class, which "Fancier value lookup" half-implies.
  Parsing is already a two-step process: split on segments and parse each one.
- Write or improvise a better backend for our fancy object store. Since many
  atoms are probably similar and there are few data types, an INTERN-like
  atom deduplication system would at least create large savings in storing
  atoms.
